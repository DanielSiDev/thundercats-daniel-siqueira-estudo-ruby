# frozen_string_literal: true

require 'json'

task default: [:run]

desc 'load the prices, order, payments, and execute it all!'
task 'run' do
  $LOAD_PATH.unshift(File.dirname(__FILE__), 'lib')
  require 'coffee_app'

  # call the app, passing the data as strings containing JSON
  coffee = CoffeeApp.new
  prices_json = coffee.prices_json
  orders_json = coffee.orders_json
  payments_json = coffee.payments_json
  result_json = coffee.call(prices_json, orders_json, payments_json)

  # # turn the JSON back into a Ruby structure
  user_balances = JSON.load(result_json)

  # # pretty print the output
  puts 'Total:'
  puts format('%-10s%-11s%-11s%-11s', 'user', 'orders', 'payments', 'balance')
  puts sprintf('--------')
  user_balances.each do |user_balance|
    order_total   = user_balance['order_total'].to_f
    payment_total = user_balance['payment_total'].to_f
    balance       = user_balance['balance'].to_f
    puts format('%-10s$%-10.2f$%-10.2f$%-10.2f', user_balance['user'], order_total, payment_total, balance)
  end
end

# frozen_string_literal: true

# Comece por aqui .....
require File.expand_path(File.join(File.dirname(__FILE__), 'controller/order_controller'))
require File.expand_path(File.join(File.dirname(__FILE__), 'controller/payment_controller'))
require File.expand_path(File.join(File.dirname(__FILE__), 'controller/price_controller'))
# Main Class
class CoffeeApp
  attr_reader :user_order_consume, :user_payments_pay, :user_payments_balance, :user_orders
  def initialize
    @order = OrderController.new
    @payment = PaymentController.new
    @price = PriceController.new
  end

  def orders_json
    @order.orders_json
  end

  def payments_json
    @payment.payments_json
  end

  def prices_json
    @price.prices_json
  end

  def build_coffee(prices_json, orders_json, payments_json)
    # Total consumido pelos Clientes
    @user_order_consume = @order.consume_total(orders_json, prices_json)
    # Total pago pelos Clientes
    @user_payments_pay = @payment.payments_total(payments_json)
    # Saldo Devedor
    @user_payments_balance = @payment.balance(orders_json, prices_json, payments_json)
    # Pedidos de cada Cliente
    @user_orders = JSON.parse(@order.total_order_user)
  end

  def call(prices_json, orders_json, payments_json)
    build_coffee(prices_json, orders_json, payments_json)
    data_list = []
    @user_orders.each do |key, _value|
      data = { "user": key, "order_total": @user_order_consume[key.to_s], "payment_total": @user_payments_pay[key.to_s], "balance": @user_payments_balance[key.to_s] }
      data_list.push(data)
    end
    data_list.to_json
  end
end

# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), '../model/file_load.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '../model/order.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '../model/product.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), './price_controller.rb'))
require 'json'
# class
class OrderController
  def initialize
    @file_path = FileLoad.new 'data/orders.json'
    @orders = @file_path.load_file
    @price = PriceController.new
  end

  def orders_json
    @orders.to_json
  end

  def order_list(orders_json)
    orders = []
    list_order = JSON.parse(orders_json, symbolize_names: true)
    list_order.each do |p|
      product = { name: p[:drink], size: p[:size], type: 'coffee' }
      order = Order.new(p[:user], product)
      orders.push(order)
    end
    orders
  end

  def order_users
    order_users = []
    totals = JSON.parse(total_order_user)
    totals.each do |key, value|
      order_users.push({ user: key, order_total: value })
    end
    order_users.to_json
  end

  def total_order_user
    total_order_client = {}
    order_list(orders_json).each do |p|
      total_order_client[p.user.name] = if total_order_client.key? p.user.name
                                          total_order_client[p.user.name] + 1
                                        else
                                          1
                                         end
    end
    total_order_client.to_json
  end

  def build_total_consume(prices_json, orders, consume)
    order = orders.user.name
    prod_name = orders.product.name
    prod_size = orders.product.size
    if consume.key? order
      line_price = @price.price_by_drink_name_size(prices_json, prod_name, prod_size)
      consume[order] = consume[order] + line_price
    else
      consume[order] = @price.price_by_drink_name_size(prices_json, prod_name, prod_size)
    end
    consume
  end

  def consume_total(order_json, prices_json)
    consume = {}
    order_list(order_json).each do |orders|
      build_total_consume(prices_json, orders, consume)
    end
    consume
  end
end

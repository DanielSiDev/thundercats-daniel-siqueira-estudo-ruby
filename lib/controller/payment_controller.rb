# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), '../model/file_load.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '../model/payment.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), './order_controller.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), './price_controller.rb'))
require 'json'
# class
class PaymentController
  def initialize
    @file_path = FileLoad.new 'data/payments.json'
    @payments = @file_path.load_file
    @order = OrderController.new
    @price = PriceController.new
  end

  def payments_json
    @payments.to_json
  end

  def payments_total(payments_json)
    paid_by_client = {}
    payments = JSON.parse(payments_json, symbolize_names: true)
    payments.each do |p|
      user = p[:user]
      u_cli = paid_by_client[user]
      amount = p[:amount]
      paid_by_client[user] = (paid_by_client.key? user) ? u_cli + amount : amount
    end
    paid_by_client
  end

  def build_balance(consume_key, consume_value, balance_user, paid)
    key = consume_key.to_s
    balance_user[key] = if consume_value > paid[key]
                          consume_value - paid[key]
                        else
                          paid[key] - consume_value
                        end
    balance_user
  end

  def balance(order_json, prices_json, payments_json)
    balance_user = {}
    consume = @order.consume_total(order_json, prices_json)
    paid = payments_total(payments_json)
    consume.each do |c_key, c_value|
      build_balance(c_key, c_value, balance_user, paid)
    end
    balance = balance_user
    balance
  end
end

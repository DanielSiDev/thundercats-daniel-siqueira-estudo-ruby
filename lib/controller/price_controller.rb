# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), '../model/file_load.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '../model/price.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '../model/product.rb'))
require 'json'
# class
class PriceController
  include Value
  def initialize
    @file_path = FileLoad.new 'data/prices.json'
    @prices = @file_path.load_file
  end

  def prices_json
    @prices.to_json
  end

  def prices_list(prices)
    prices_list = []
    prices.each do |key, value|
      price = Value::Price.new({ name: key, value: value })
      prices_list.push(price)
    end
    prices_list
  end

  def prices_product(prices_json)
    price_list_product = []
    JSON.parse(prices_json, symbolize_names: true).each do |p|
      prices = prices_list(p[:prices])
      name = p[:drink_name]
      type = 'coffee'
      price_product = Product.new({ name: name, type: type, prices: prices })
      price_list_product.push(price_product)
    end
    price_list_product
  end

  def price_by_drink_name_size(prices_json, drink_name, size_name)
    @drink_name = drink_name.to_s
    @size_name = size_name.to_s
    prices_product(prices_json).each do |p|
      next unless p.name == @drink_name

      p.prices.each do |pri|
        return pri.value if pri.value.to_s.length > 1
      end
    end
  end
end

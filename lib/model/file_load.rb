# frozen_string_literal: true

require 'json'
# This class make set/get the file path and recovery data from json
class FileLoad
  attr_accessor :path

  def initialize(path_name)
    @path = File.expand_path(path_name)
  end

  def load_file
    file = File.read(@path)
    JSON.parse(file) # symbolize_names:true
  end
end

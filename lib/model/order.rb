# frozen_string_literal: true
require File.expand_path(File.join(File.dirname(__FILE__), './client'))
require File.expand_path(File.join(File.dirname(__FILE__), './product'))

# Model fro Orders
class Order
include User
  attr_accessor :user, :product
  def initialize(user, product)
    @user    = User::Client.new(user)
    @product = Product.new(product) 
  end
end

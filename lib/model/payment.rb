# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), './client'))
# Model for Payment
class Payment
  include User

  attr_accessor :user, :amount_paid
  def initialize(payment)
    @user = User::Client.new(payment[:user])
    @amount_paid = payment[:amount]
  end
end

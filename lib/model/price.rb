module Value
  class Price
    attr_accessor :name, :value
    def initialize(price)
      @name  = price[:name]
      @value = price[:value]
    end
  end  
end

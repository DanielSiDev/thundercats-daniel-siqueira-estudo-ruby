
require File.expand_path(File.join(File.dirname(__FILE__), './price'))
require File.expand_path(File.join(File.dirname(__FILE__), './product_type'))

    class Product
      include Type
      include Value
      attr_accessor :prices, :name, :size, :type  
        def initialize(product)
          @name  = product[:name]
          @size  = product.key?("size")? '' : product[:size]
          @type  = Type::Product_Type.new({type:product[:type]}) 
          @prices = product.key?("prices")? {} :  product[:prices] 
        end
    end    


  #criar metodo pra construir um menu de cafes com seus preços


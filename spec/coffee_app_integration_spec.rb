# frozen_string_literal: true

require 'spec_helper'
require 'coffee_app'
require 'model/order'
require 'model/client'
require 'model/product'
require 'model/product_type'

require 'json'
RSpec.describe 'integration' do
  let(:prices_json) do
    <<-JSON
    [{"drink_name":"short espresso","prices":{"small":3.0}},{"drink_name":"latte","prices":{"small":3.5,"medium":4.0,"large":4.5}},{"drink_name":"flat white","prices":{"small":3.5,"medium":4.0,"large":4.5}},{"drink_name":"long black","prices":{"small":3.25,"medium":3.5}},{"drink_name":"mocha","prices":{"small":4.0,"medium":4.5,"large":5.0}},{"drink_name":"supermochacrapucaramelcream","prices":{"large":5.0,"huge":5.5,"mega":6.0,"ultra":7.0}}]
    JSON
  end

  let(:orders_json) do
    <<-JSON
    [{"user":"coach","drink":"long black","size":"medium"},{"user":"ellis","drink":"long black","size":"small"},{"user":"rochelle","drink":"flat white","size":"large"},{"user":"coach","drink":"flat white","size":"large"},{"user":"zoey","drink":"long black","size":"medium"},{"user":"zoey","drink":"short espresso","size":"small"},{"user":"nick","drink":"mocha","size":"large"},{"user":"bill","drink":"supermochacrapucaramelcream","size":"ultra"},{"user":"ellis","drink":"mocha","size":"small"},{"user":"rochelle","drink":"short espresso","size":"small"},{"user":"francis","drink":"long black","size":"small"},{"user":"coach","drink":"mocha","size":"medium"},{"user":"coach","drink":"short espresso","size":"small"},{"user":"rochelle","drink":"long black","size":"medium"},{"user":"bill","drink":"mocha","size":"medium"},{"user":"ellis","drink":"mocha","size":"small"},{"user":"louis","drink":"mocha","size":"small"},{"user":"coach","drink":"latte","size":"medium"},{"user":"zoey","drink":"flat white","size":"large"},{"user":"ellis","drink":"latte","size":"small"},{"user":"louis","drink":"latte","size":"medium"},{"user":"zoey","drink":"short espresso","size":"small"},{"user":"louis","drink":"long black","size":"small"},{"user":"zoey","drink":"flat white","size":"large"},{"user":"rochelle","drink":"supermochacrapucaramelcream","size":"large"},{"user":"coach","drink":"flat white","size":"large"},{"user":"francis","drink":"mocha","size":"small"},{"user":"francis","drink":"short espresso","size":"small"},{"user":"francis","drink":"short espresso","size":"small"},{"user":"ellis","drink":"flat white","size":"small"},{"user":"coach","drink":"long black","size":"small"},{"user":"coach","drink":"short espresso","size":"small"},{"user":"ellis","drink":"supermochacrapucaramelcream","size":"huge"},{"user":"ellis","drink":"short espresso","size":"small"},{"user":"coach","drink":"latte","size":"small"},{"user":"nick","drink":"short espresso","size":"small"},{"user":"ellis","drink":"short espresso","size":"small"},{"user":"rochelle","drink":"latte","size":"large"},{"user":"coach","drink":"latte","size":"medium"},{"user":"rochelle","drink":"long black","size":"small"},{"user":"ellis","drink":"flat white","size":"large"},{"user":"bill","drink":"latte","size":"large"},{"user":"rochelle","drink":"supermochacrapucaramelcream","size":"mega"},{"user":"coach","drink":"mocha","size":"large"},{"user":"bill","drink":"supermochacrapucaramelcream","size":"mega"},{"user":"francis","drink":"long black","size":"small"},{"user":"coach","drink":"long black","size":"small"},{"user":"francis","drink":"flat white","size":"small"},{"user":"coach","drink":"long black","size":"medium"},{"user":"bill","drink":"long black","size":"medium"},{"user":"bill","drink":"supermochacrapucaramelcream","size":"huge"},{"user":"ellis","drink":"supermochacrapucaramelcream","size":"ultra"},{"user":"rochelle","drink":"short espresso","size":"small"},{"user":"louis","drink":"mocha","size":"large"},{"user":"coach","drink":"supermochacrapucaramelcream","size":"large"},{"user":"nick","drink":"short espresso","size":"small"},{"user":"bill","drink":"mocha","size":"medium"},{"user":"ellis","drink":"short espresso","size":"small"},{"user":"nick","drink":"flat white","size":"large"},{"user":"zoey","drink":"supermochacrapucaramelcream","size":"ultra"},{"user":"louis","drink":"flat white","size":"large"},{"user":"louis","drink":"long black","size":"small"},{"user":"nick","drink":"short espresso","size":"small"},{"user":"rochelle","drink":"latte","size":"medium"},{"user":"louis","drink":"mocha","size":"small"},{"user":"louis","drink":"long black","size":"medium"},{"user":"coach","drink":"supermochacrapucaramelcream","size":"mega"},{"user":"francis","drink":"long black","size":"medium"},{"user":"louis","drink":"flat white","size":"small"},{"user":"bill","drink":"supermochacrapucaramelcream","size":"mega"},{"user":"zoey","drink":"latte","size":"small"},{"user":"ellis","drink":"flat white","size":"small"},{"user":"zoey","drink":"flat white","size":"small"},{"user":"ellis","drink":"short espresso","size":"small"},{"user":"rochelle","drink":"flat white","size":"medium"},{"user":"ellis","drink":"supermochacrapucaramelcream","size":"large"},{"user":"nick","drink":"latte","size":"small"},{"user":"coach","drink":"latte","size":"medium"},{"user":"nick","drink":"mocha","size":"medium"},{"user":"louis","drink":"latte","size":"large"},{"user":"louis","drink":"supermochacrapucaramelcream","size":"mega"},{"user":"francis","drink":"supermochacrapucaramelcream","size":"mega"},{"user":"zoey","drink":"latte","size":"medium"},{"user":"francis","drink":"latte","size":"medium"},{"user":"coach","drink":"flat white","size":"small"},{"user":"coach","drink":"supermochacrapucaramelcream","size":"large"},{"user":"ellis","drink":"short espresso","size":"small"},{"user":"louis","drink":"flat white","size":"medium"},{"user":"bill","drink":"short espresso","size":"small"},{"user":"nick","drink":"flat white","size":"large"},{"user":"zoey","drink":"mocha","size":"large"},{"user":"rochelle","drink":"long black","size":"medium"},{"user":"zoey","drink":"latte","size":"small"},{"user":"rochelle","drink":"short espresso","size":"small"},{"user":"francis","drink":"mocha","size":"large"},{"user":"bill","drink":"long black","size":"small"},{"user":"francis","drink":"supermochacrapucaramelcream","size":"ultra"},{"user":"rochelle","drink":"supermochacrapucaramelcream","size":"large"},{"user":"ellis","drink":"supermochacrapucaramelcream","size":"ultra"},{"user":"nick","drink":"supermochacrapucaramelcream","size":"huge"}]
    JSON
  end

  let(:payments_json) do
    <<-JSON
    [{"user":"coach","amount":27},{"user":"rochelle","amount":22},{"user":"bill","amount":41},{"user":"zoey","amount":0},{"user":"bill","amount":36},{"user":"nick","amount":48},{"user":"coach","amount":42},{"user":"rochelle","amount":36},{"user":"nick","amount":48},{"user":"francis","amount":45},{"user":"rochelle","amount":27},{"user":"francis","amount":31},{"user":"zoey","amount":9},{"user":"nick","amount":47},{"user":"louis","amount":12},{"user":"rochelle","amount":10},{"user":"zoey","amount":43},{"user":"francis","amount":36},{"user":"zoey","amount":49},{"user":"ellis","amount":24}]
    JSON
  end

  let(:expected_result_json) do
    <<-JSON
      [
        {"user":"coach", "order_total":66.5, "payment_total":69, "balance":2.5},
        {"user":"ellis", "order_total":60.25, "payment_total":24, "balance":36.25},
        {"user":"rochelle", "order_total":47.75, "payment_total":95, "balance":47.25},
        {"user":"zoey", "order_total":39.25, "payment_total":101, "balance":61.75},
        {"user":"nick", "order_total":32.5, "payment_total":143, "balance":110.5},
        {"user":"bill", "order_total":41.0, "payment_total":77, "balance":36.0},
        {"user":"francis", "order_total":40.75, "payment_total":112, "balance":71.25},
        {"user":"louis", "order_total":44.25, "payment_total":12, "balance":32.25}
      ]
    JSON
  end

  describe CoffeeApp do
    it 'should to load prices from json' do
      coffee = CoffeeApp.new
      prices = coffee.prices_json
      pr_json = JSON.parse(prices_json).to_json
      expect(prices).to eq pr_json
    end

    it 'should to load orders from json' do
      coffee = CoffeeApp.new
      orders = coffee.orders_json
      o_json = JSON.parse(orders_json).to_json
      expect(orders).to eq o_json
    end

    it 'should to load payments from json' do
      coffee = CoffeeApp.new
      payments = coffee.payments_json
      p_json = JSON.parse(payments_json).to_json
      expect(payments).to eq p_json
    end

    subject(:result) do
      coffee = CoffeeApp.new
      prices_json = coffee.prices_json
      orders_json = coffee.orders_json
      payments_json = coffee.payments_json
      json_result = coffee.call(prices_json, orders_json, payments_json)
      JSON.load(json_result)
    end

    it 'outputs JSON in expected form' do
      expect(result).to eq JSON.load(expected_result_json)
    end
    it 'has a bunch of users who have ordered coffee' do
      expect(result[0]['user']).to eq 'coach'
      expect(result[1]['user']).to eq 'ellis'
      expect(result[2]['user']).to eq 'rochelle'
      expect(result[3]['user']).to eq 'zoey'
    end
    it 'has payment totals for each user' do
      expect(result[0]['payment_total']).to eq 69  # coach
      expect(result[1]['payment_total']).to eq 24  # ellis
      expect(result[2]['payment_total']).to eq 95  # rochelle
      expect(result[3]['payment_total']).to eq 101 # zoey
      expect(result[4]['payment_total']).to eq 143 # nick
      expect(result[5]['payment_total']).to eq 77  # bill
      expect(result[6]['payment_total']).to eq 112 # francis
      expect(result[7]['payment_total']).to eq 12  # louis
    end
    it 'has order totals for each user' do
      expect(result[0]['order_total']).to eq 66.5 # coach
      expect(result[1]['order_total']).to eq 60.25 # ellis
      expect(result[2]['order_total']).to eq 47.75 # rochelle
      expect(result[3]['order_total']).to eq 39.25 # zoey
    end
    it 'has current balance for each user' do
      expect(result[0]['balance']).to eq 2.5   # coach
      expect(result[1]['balance']).to eq 36.25 # ellis
      expect(result[2]['balance']).to eq 47.25 # rochelle
      expect(result[3]['balance']).to eq 61.75 # zoey
    end
  end
end

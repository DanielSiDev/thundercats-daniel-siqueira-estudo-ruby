# frozen_string_literal: true

require 'spec_helper'
require 'model/order'
require 'model/client'
require 'model/product'
require 'model/product_type'
require 'controller/order_controller'
require 'controller/price_controller'
require 'controller/order_controller'

RSpec.describe 'Order Controller' do
  it 'has order list for each user' do
    coffee = OrderController.new
    orders = coffee.orders_json
    result = coffee.order_list(orders)
    expect(result[0].user.name).to eq 'coach' # coach
    expect(result[0].product.name).to eq 'long black' # long black
    expect(result[0].product.size).to eq 'medium' # medium
    expect(result[0].product.type.name).to eq 'coffee' # coffee
  end

  it 'has order user total for each user' do
    coffee = OrderController.new
    result = JSON.parse(coffee.total_order_user)
    expect(result['coach']).to eq 18
    expect(result['ellis']).to eq 16
    expect(result['rochelle']).to eq 13
    expect(result['zoey']).to eq 11
    expect(result['nick']).to eq 9
    expect(result['bill']).to eq 10
    expect(result['francis']).to eq 11
    expect(result['louis']).to eq 12
  end

  it 'should to consume total by user' do
    coffee = OrderController.new
    prices = PriceController.new
    prices_json = prices.prices_json
    orders_json = coffee.orders_json
    result = coffee.consume_total(orders_json, prices_json)
    expect(result['coach']).to eq 66.5
    expect(result['ellis']).to eq 60.25
    expect(result['rochelle']).to eq 47.75
    expect(result['zoey']).to eq 39.25
    expect(result['nick']).to eq 32.5
    expect(result['bill']).to eq 41.0
    expect(result['francis']).to eq 40.75
    expect(result['louis']).to eq 44.25
  end
  it 'should to build consume total by user' do
    coffee = OrderController.new
    prices = PriceController.new
    prices_json = prices.prices_json

    user = User::Client.new('Nick')
    type = Type::Product_Type.new({ type: 'Beer' })
    name = 'mocha'
    size = 'large'
    prices = 5.0
    product = { type: type.name, name: name, prices: prices, size: size }
    order = Order.new(user.name, product)
    result = coffee.build_total_consume(prices_json, order, {})
    expect(result[user.name]).to eq 4.0
  end
end

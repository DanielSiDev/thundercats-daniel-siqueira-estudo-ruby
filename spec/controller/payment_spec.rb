# frozen_string_literal: true

require 'spec_helper'
require 'controller/payment_controller'
require 'controller/price_controller'
require 'controller/order_controller'

RSpec.describe 'Payment Controller' do
  it 'has payments total for each user' do
    coffee = PaymentController.new
    payments_json = coffee.payments_json
    result = coffee.payments_total(payments_json)
    expect(result['coach']).to eq 69
    expect(result['ellis']).to eq 24
    expect(result['rochelle']).to eq 95
    expect(result['zoey']).to eq 101
    expect(result['nick']).to eq 143
    expect(result['bill']).to eq 77
    expect(result['francis']).to eq 112
    expect(result['louis']).to eq 12
  end

  it 'should to balance' do
    coffee = PaymentController.new
    payments_json = coffee.payments_json
    prices = PriceController.new
    prices_json = prices.prices_json
    orders = OrderController.new
    orders_json = orders.orders_json
    result = coffee.balance(orders_json, prices_json, payments_json)
    expect(result['coach']).to eq 2.5
    expect(result['ellis']).to eq 36.25
    expect(result['rochelle']).to eq 47.25
    expect(result['zoey']).to eq 61.75
    expect(result['nick']).to eq 110.5
    expect(result['bill']).to eq 36.0
    expect(result['francis']).to eq 71.25
    expect(result['louis']).to eq 32.25
  end

  it 'should to build balance' do
    coffee = PaymentController.new
    payments_json = coffee.payments_json
    key = 'ellis'
    value = 60.25
    balance_user = {}
    paid = coffee.payments_total(payments_json)
    result = coffee.build_balance(key, value, balance_user, paid)
    expect(result['ellis']).to eq 36.25
  end
end

# frozen_string_literal: true

require 'spec_helper'
require 'controller/price_controller'

RSpec.describe 'Price Controller' do
  it 'has price list for each user' do
    coffee = PriceController.new
    prices = coffee.prices_json
    prices_list = JSON.parse(prices, symbolize_names: true)
    result = coffee.prices_list(prices_list[0][:prices])
    expect(result[0].name).to eq :small
    expect(result[0].value).to eq(3.0)
  end

  it 'has price product for each user' do
    coffee = PriceController.new
    prices = coffee.prices_json
    result = coffee.prices_product(prices)
    expect(result[0].name).to eq('short espresso')
    expect(result[0].type.name).to eq('coffee')
    expect(result[0].prices[0].name).to eq(:small)
    expect(result[0].prices[0].value).to eq(3.0)
  end

  it 'has  price by drink name size for each user' do
    coffee = PriceController.new
    prices = coffee.prices_json
    result = coffee.price_by_drink_name_size(prices, 'short espresso', 'small')
    expect(result).to eq(3.0)
  end
end

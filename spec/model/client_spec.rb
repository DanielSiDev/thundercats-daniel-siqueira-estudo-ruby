# frozen_string_literal: true

require 'spec_helper'
require 'model/client'
RSpec.describe 'Client Model' do
  it 'should to Client name' do
    cli = User::Client.new('Nick')
    expect(cli.name).to eq('Nick')
  end
end

# frozen_string_literal: true

require 'spec_helper'
require 'model/client'
require 'model/product'
require 'model/product_type'

RSpec.describe 'Order ' do
  let(:user) { User::Client.new('Nick') }
  let(:type) { Type::Product_Type.new({ type: 'Beer' }) }
  let(:name) { 'mocha' }
  let(:size) { 'large' }
  let(:prices) { 5.0 }
  let(:product) { { type: type.name, name: name, prices: prices, size: size } }
  it 'should to New Order' do
    order = Order.new(user.name, product)

    expect(order.user.name).to eq(user.name)
    expect(order.product.name).to eq(product[:name])
    expect(order.product.size).to eq(product[:size])
    expect(order.product.type.name).to eq(product[:type])
    expect(order.product.prices).to eq(product[:prices])
    expect(1).to eq(1)
  end
end

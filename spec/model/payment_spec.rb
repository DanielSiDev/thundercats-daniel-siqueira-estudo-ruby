# frozen_string_literal: true

require 'spec_helper'
require 'model/client'
require 'model/payment'

RSpec.describe 'Payment ' do
  let(:user) { User::Client.new('Nick') }
  let(:amount) { 77.8 }

  it 'should to New Payment' do
    pay = Payment.new({
                        amount: amount,
                        user: user.name
                      })
    expect(pay.user.name).to eq(user.name)
    expect(pay.amount_paid).to eq(amount)
  end
end

# frozen_string_literal: true

require 'spec_helper'
require 'model/price'

RSpec.describe 'Price Model' do
  it 'should to Price' do
    pri = Value::Price.new({ name: 'large', value: 5.0 })
    expect(pri.name).to eq('large')
    expect(pri.value).to eq(5.0)
  end
end

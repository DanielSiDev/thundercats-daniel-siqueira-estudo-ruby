# frozen_string_literal: true

require 'spec_helper'
require 'model/product_type'
require 'model/product'

RSpec.describe 'Product ' do
  let(:type) { Type::Product_Type.new({ type: 'Beer' }) }
  let(:name) { 'mocha' }
  let(:size) { 'large' }
  let(:prices) { 5.0 }

  it 'should to New Product' do
    prod = Product.new({
                         type: type.name,
                         name: name,
                         prices: prices,
                         size: size
                       })
    expect(prod.name).to eq(name)
    expect(prod.type.name).to eq(type.name)
    expect(prod.size).to eq(size)
    expect(prod.prices).to eq(prices)
  end
end

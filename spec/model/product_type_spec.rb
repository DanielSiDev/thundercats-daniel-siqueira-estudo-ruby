# frozen_string_literal: true

require 'spec_helper'
require 'model/product_type'

RSpec.describe 'Product Type' do
  it 'should to Product Type Name' do
    pro = Type::Product_Type.new({ type: 'Beer' })
    expect(pro.name).to eq('Beer')
  end
end
